This is the basic ins-and-outs of my website code,
hopefully to help those interested in setting up a 
Django website in Python. For the front-end, eye-candy
 stuff, I used HTML5. I also took advantage of CSS to handle the more 
interesting things. 

For box transitions and the slider I 
used Javascript and Jquery. Somethings are just easier with them.
 Let me know what you think --> herman.rogers@gmail.com 
or if you have questions about setting up your own site!

To checkout this website locally make sure you have Django 1.5 and 
Python 2.7.5 installed and just clone into the repository. Once that's 
done point your terminal to your folder ($cd /home/your/file) and run 

$python manage.py runserver
You're all set!

To see the site in action hop on over to www.boomerweb.me
